//
//  ViewController.m
//  myself
//
//  Created by dronnefjord on 2015-01-21.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UIButton *magicButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)randomBackgroundColor:(id)sender {
    self.background.backgroundColor = [UIColor colorWithRed:drand48()
                                                      green:drand48()
                                                       blue:drand48()
                                                      alpha:1.0f];
}

@end
